
CC = gcc
NAME="powerlog"

INSTALL_HEADER="/usr/local/include/$(NAME)/"
INSTALL_BIN="/usr/local/bin/$(NAME)"

O = main.o k2280s.o tmcdev.o scpiINet.o k2110.o

%.o: %.c
	$(CC) -g -c $< -o $*.o

server: $(O)
	$(CC) $(O) -o server -lpthread -lncurses

client: client.o
	$(CC) client.o -o client

header:
	mkdir -p $(INSTALL_HEADER)
	cp message.h $(INSTALL_HEADER)

install: server
	mkdir -p $(INSTALL_HEADER)
	mkdir -p $(INSTALL_BIN)
	cp message.h $(INSTALL_HEADER)
	cp server $(INSTALL_BIN)

uninstall:
	rm -rf $(INSTALL_HEADER)
	rm $(INSTALL_BIN)/server

all: server client

clean:
	rm server
	rm *.o

.PHONY: all clean install uninstall header
