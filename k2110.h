
#ifndef K2110
#define K2110

#include <pthread.h>
#include <ncurses.h>

#include "tmcdev.h"

struct k2110{
    struct tmcdev *tdev;
    char *name; 
    int active;

    volatile int num_avg_val;
    volatile float acc_val;
    pthread_mutex_t ncl;

    pthread_t thread;
    pthread_mutex_t read_lock;
    WINDOW *win;
};


void k2110_init(struct k2110** ddev, char* dfile, char* name, int num);
void k2110_destroy(struct k2110* ddev);

float k2110_read_avg( volatile struct k2110 * dev );

#endif // K2210
