
#ifndef MESSAGE
#define MESSAGE

#include <stdint.h>

#define MSG_START 0x00
#define MSG_STOP  0x01
#define MSG_TXLOG 0x02

struct k2280s_msg{
    uint32_t id;
    uint32_t len; // Length for data to receive after this
} __attribute__((packed));
typedef struct k2280s_msg k2280s_msg_t;

#endif // MESSAGE
