
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "k2110.h"
#include "common.h"

#define MODULE "K2110 "

#define NUM_AVG 20


float k2110_read_avg( volatile struct k2110 * dev )
{
    float retval;
    pthread_mutex_lock(&dev->read_lock);

    if(dev->num_avg_val)
    {
        retval = dev->acc_val / (float) dev->num_avg_val;
        dev->acc_val = 0.0F;
        dev->num_avg_val = 0;
    }

    pthread_mutex_unlock(&dev->read_lock);

    return retval;
}

static void* k2110_thread(void* args){
    
    struct k2110 *dev;
    char rsp[5000];
    float val, avg;
    float agr[NUM_AVG];
    int i, j;

    for(i=0; i<NUM_AVG; i++)
        agr[i] = 0.0F;

    i = 0;

    dev = (struct k2110 *) args;

    LOG("Thread online\n");
    while(dev->active){
        
        tmcScpiCmd(dev->tdev, ":READ1?", RESPONSE, rsp);

        val = atof(rsp);

        pthread_mutex_lock(&dev->read_lock);
        dev->num_avg_val++;
        dev->acc_val += val;
        pthread_mutex_unlock(&dev->read_lock);

        agr[i++ % NUM_AVG]  = val;
        avg = 0.0F;

        for(j=0; j< (i < NUM_AVG ? i : NUM_AVG); j++)
            avg += agr[j];

        avg /= i < NUM_AVG ? i : NUM_AVG;


        pthread_mutex_lock(&dev->ncl);
	if( dev->num_avg_val == 20 ){
        	mvwprintw(dev->win, 1, 1, "%.3f V", dev->acc_val / dev->num_avg_val);
//		dev->num_avg_val = 0;
//		dev->acc_val = 0;
	}
        wrefresh(dev->win);
        pthread_mutex_unlock(&dev->ncl);
    }
    LOG("Thread exiting\n");
}

void k2110_init(struct k2110** ddev, char* dfile, char* name, int num){

    struct k2110 *dev;
    WINDOW *win;

    *ddev = malloc(sizeof(struct k2110));
    dev = *ddev;

    initTmcDev(&dev->tdev, name, dfile); // Initialise TMC device

    dev->name = malloc(sizeof(char) * (strlen(name) + 1));
    strcpy(dev->name, name);

    win = newwin(3, 15, 5+num*5, 95); // Voltage monitor window
    box(win, 0, 0);

    dev->active = 1;
    dev->win = win;

    pthread_mutex_init(&dev->ncl, NULL);
    pthread_mutex_lock(&dev->ncl);
    mvwprintw(dev->win, 0, 1, " %s ", dev->name);
    wrefresh(dev->win);
    pthread_mutex_unlock(&dev->ncl);


    if(pthread_create(&dev->thread, NULL, k2110_thread, (void*) dev)){
        ERROR("Could not create server thread (%s)\n", strerror(errno));
    }

    dev->num_avg_val = 0;
    dev->acc_val = 0.0F;
    pthread_mutex_init( &dev->read_lock, NULL );

    LOG("Initialised %s on %s\n", dev->name, dfile);
}

void k2110_destroy(struct k2110* dev){

    pthread_mutex_destroy( &dev->read_lock );

    free(dev->name);
    free(dev);
    
}

