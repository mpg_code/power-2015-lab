
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <linux/usb/tmc.h>
#include <time.h>
//#include <libexplain/ioctl.h>
#include <ncurses.h>
#include <dirent.h>
#include <signal.h>
#include <time.h>

#include "k2280s.h"
#include "k2110.h"

#define MODULE "MAIN"

struct k2110* vmons[10];
int num_vmons = 0;

FILE *logg;
pthread_mutex_t logg_lock;
pthread_mutex_t ncl;
struct timespec clock_s;
struct monitor *mon;
WINDOW *win;

struct monitor{
    struct k2280s *k2280s;
};

static void print_help()
{

    fprintf(stdout, "\nUsage: ./2280smon [options]\n");

    fprintf(stdout, "\t--server port:IP:name\t\tCreate server socket on PORT, and a k2280s logger on IP with NAME.\n");
    fprintf(stdout, "\t--k2110 name:devfile \t\tCreate a k2110 voltmeter monitor on DEVFILE with NAME.\n");
    fprintf(stdout, "\t--query              \t\tQuery all /dev/usbtmcXX devices.\n");

    exit(EXIT_SUCCESS);
}

void sigsegv_handler(int signum){

    close(mon->k2280s->sdev->sockfd);
    endwin();
    fprintf(stderr, "SIGSEGV HANDLER CALLED\n");
}

int main(int argc, char** argv){

    char c;
    int optind, row = 0, ret;

//    signal(SIGSEGV, sigsegv_handler);

    clock_gettime(CLOCK_REALTIME, &clock_s);
    mon = malloc(sizeof(struct monitor));
    pthread_mutex_init(&logg_lock, NULL);
    pthread_mutex_init(&ncl,       NULL);

    /* Voltage window */

    struct option options[] = 
    {
        {"server",  required_argument, 0, 's'}, // K2280S
        {"k2110",   required_argument, 0, 'k'}, // K2110
        {"help",    optional_argument, 0, 'h'},
        {"query",   optional_argument, 0, 'q'},

        {0,0,0,0}
    };

    win = initscr();
//    ret = use_window(win, NULL, NULL);

    while((c = getopt_long(argc, argv, "s:k:hq", options, &optind)) != -1){

        switch(c){
            case 'k':
                {
                    char *dfile, *name;

                    name  = strtok(optarg, ":");
                    dfile = strtok(NULL, ":");

                    k2110_init(&vmons[row], dfile, name, row);
                    row++;
                    num_vmons++;
                }

                break;

            case 's':
                {   
                    char *dfile, *name;
                    int port;

                    port = atoi(strtok(optarg, ":"));
                    dfile = strtok(NULL, ":");
                    name = strtok(NULL, ":");

                    k2280s_init(&mon->k2280s, port, name, dfile);
                }

                break;

            case 'h':

                endwin();
                print_help();

                break;

            case 'q':
                {
                    char rsp[1000];
                    char path[1000];
                    DIR *d;
                    struct dirent *dir;
                    endwin();
                    d = opendir("/dev");
                    while((dir = readdir(d)) != NULL){
                        if(!(strncmp(dir->d_name, "usbtmc", 6))){
                            struct tmcdev *tdev;
                            sprintf(path, "%s/%s", "/dev", dir->d_name);
                            initTmcDev(&tdev, dir->d_name, path);
                            tmcScpiCmd(tdev, "*IDN?", RESPONSE, rsp);
                            destroyTmcDev(tdev);
                            INFO("%s: %s", dir->d_name, rsp);
                        }
                    }
                    closedir(d);
                }

                exit(EXIT_SUCCESS);
                break;
        }
    }


    while(1){
        //refresh();
    }

    k2280s_destroy(mon->k2280s);
    free(mon);

    return 0;
}
