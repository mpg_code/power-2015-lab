
#include <stdint.h>

#include "scpiINet.h"

#define MODULE "SCPIINET"

/* Contains message header */
static char rve[8];

/* Writes commands, with or without response. Command must
 * include trailing newline \n or \r\n as per the 2280S
 * documentation. 
 *
 * */

int scpiINetWrite(struct scpiINetDev *dev, char* cmd){

    uint16_t len;    // Length of command
    int ret;

    len = (uint16_t) strlen(cmd);
    rve[6] = (char)  (len & 0x00FF);
    rve[7] = (char) ((len & 0xFF00) >> 8);

//    if(!strcmp(cmd, ":TRACE1:CLEAR\n")){
    if(!strstr(cmd, "?")){
        rve[4] = 0x01;
    }
    else
        rve[4] = 0x04;

//    LOG("Length %u %u\n", (uint8_t) rve[6], (uint8_t) rve[7]);

    if(len == 0)
        ERROR("Cannot send zero length command.\n");

    /* First write command header */
    ret = write(dev->sockfd, rve, 8);
    if(ret == -1){
        ERROR("<write,header>: %s\n", strerror(errno));
    }
    else if(ret < 8)
        ERROR("Failed to write header (wrote %d bytes instead of 8)\n", ret);

    /* Write command */
    ret = write(dev->sockfd, cmd, (int) len);
    if(ret == -1){
        ERROR("<write,body>:%s\n", strerror(errno));
    }
    else if(ret < len)
        ERROR("Failed to write body (wrote %d bytes instead of %u)\n", ret, len);

    return ret;
}

/*
 * Messages from the K2280S are basically embedded in the
 * stream of data which is sent. A TCP packet can basically
 * consist of several embedded headers of the form 'f0 f0 f0
 * ..'. Care must be taken to read embedded data from across
 * TCP messages.
 * */
int scpiINetRead(struct scpiINetDev *dev, char* cmd, char *rsp){

    char rvp[8];        // Command response header
    uint16_t len;       // Length of reply
    int ret;
    int rx, rxl;        // How much has been read so far

    rx = 0;

    /* Write the request */
    LOG("Requesting %s\n", cmd);
    scpiINetWrite(dev, cmd);
//    LOG("Request written\n");

    /* We need to read messages from the server until we
     * detect a trailing 0x0a (newline), marking the end of
     * transmission */
    while(1){

        /* Read header to find length of response, to again read
         * the entire body */
        rxl = 0;
        do{
            ret = read(dev->sockfd, rvp+rxl, 8-rxl);
            if(ret == -1){
                ERROR("<read,header>: %s\n", strerror(errno));
            }
            rxl += ret;
        } while(rxl != 8);

        // Length is in reply byte numer 7
        len =   (uint16_t) (rvp[6] & 0xFF);
        len |= ((uint16_t) rvp[7]) << 8;

//        LOG("Length %u (0x%x, 0x%x)\n", len, rvp[7], rvp[6]);

        /* Read until all data is available */
        rxl = 0;
        do{
            ret = read(dev->sockfd, rsp+rx+rxl, ((int) len) - rxl);
            if(ret == -1){
                ERROR("<read,body>:%s\n", strerror(errno));
            }
            rxl += ret;
        } while(rxl != (int) len);

        rx += rxl;

        rsp[rx] = '\0';
//        LOG("Read so far (%d)\n", rx);
//        LOG("Read so far (%d): %s\n", rx, rsp);

        /* Check for end of stream */
        if(rsp[rx-1] == 0x0a)
            break;
    }

    rsp[rx] = '\0';
    return rx;
    
}

/* Initialisation inspired from
 * http://www.linuxhowtos.org/C_C++/socket.htm
 * */
void initScpiINetDev(struct scpiINetDev **dev_h, char* name, char* netaddr){

    int portno;

    struct hostent *server;
    struct sockaddr_in serv_addr;
    struct scpiINetDev *dev;

    int ret;
    char buf[5000];

    dev = malloc(sizeof(struct scpiINetDev));
    *dev_h = dev;

    /* Fill in "request" header. Most of these fields do not
     * give any meaning, except for the size field. */
    rve[0] = 0xf0;
    rve[1] = 0xf0;
    rve[2] = 0xf0;
    rve[3] = 0xf0;

    rve[4] = 0x04; // This one changes to 0x01 in some cases 
    rve[5] = 0x01;
    rve[6] = 0x06; // Size LSB
    rve[7] = 0x00; // Size MSB

    /* Initialise connection etc */
    dev->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(dev->sockfd < 0)
        ERROR("Error opening socket");
    portno = 5051; // :>

    server = gethostbyname(netaddr);
    if(server==NULL)
        ERROR("NO SUCH HOST");

    bzero((char*) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy(
            (char*) server->h_addr,
            (char*) &serv_addr.sin_addr.s_addr,
            server->h_length
         );
    serv_addr.sin_port = htons(portno);

    if(connect(dev->sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0){
        ERROR("Error connecting (%s)", strerror(errno));
    }   

    ret = read(dev->sockfd, buf, 5000);
    if(ret == -1)
        fprintf(stdout, "Read %d, %s\n", ret, strerror(errno));

    /* Server may reply with "DENIED" if another client is
     * connected! */
    if(strcmp(buf, "ACCEPT")){
        ERROR("Server replied with %s, exiting..\n", buf);
    }

}

/* Clean up */
void destroyScpiINetDev(struct scpiINetDev *dev_h){
    
    close(dev_h->sockfd);
    free(dev_h);
}
