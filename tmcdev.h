
#ifndef TMCDEV
#define TMCDEV

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <stdio.h>
#include <errno.h>

#define RESPONSE 1
#define NO_RESPONSE 0

struct tmcdev{
    char* name;     // Name of device (nickname)
    char* d_file;   // Device file (e.g. /dev/usbtmcX)

    int fd;
};

void tmcScpiCmd(struct tmcdev *dev, char* cmd, int response, char *rsp);
int tmcScpiRead(struct tmcdev *dev, char* cmd, char *rsp);
void tmcReset(struct tmcdev *dev_h);

void initTmcDev(struct tmcdev **dev_h, char* name, char* d_file);
void destroyTmcDev(struct tmcdev *dev_h);

#endif // TMCDEV
