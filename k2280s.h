#ifndef MON2280S
#define MON2280S

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#include "scpiINet.h"

struct k2280s{
    struct scpiINetDev *sdev;
    int s_port;
    int server_fd;
    int client_fd;
    int has_client;
    volatile int log_active;
    int log_size;

    struct sockaddr_in server_addr;
    struct sockaddr_in client_addr;
    socklen_t client_addr_len;

    FILE *log_fp;

    WINDOW *win;

    pthread_t sthread; // Server thread
    pthread_t dthread; // Data thread
    pthread_barrier_t barrier;
};

void k2280s_init(struct k2280s** mon, int port, char* name, char* d_file);
void k2280s_destroy(struct k2280s* mon);

#endif // MON2280S
