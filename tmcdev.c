
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <linux/usb/tmc.h>
#include <sys/ioctl.h>

#include "tmcdev.h"
#include "common.h"

#define MODULE "TMCDEV"

void tmcReset(struct tmcdev *dev){

  /*  if(ioctl(dev->fd, USBTMC_IOCTL_CLEAR)){
        ERROR("Unable to clear (return %s)", strerror(errno));
    }*/

}

int tmcScpiRead(struct tmcdev *dev, char* cmd, char *rsp){

    char reqrsp[50000];
    int ret;
    int retry = 3;

tsc_retry:
    ret = write(dev->fd, cmd, strlen(cmd));
    if(ret == -1){
        if(errno == ETIMEDOUT){
            if(--retry < 0){
                ERROR("measurement read failed (%s)\n", strerror(errno));
            }else{
                INFO("Timeout (retry %d). Attempting reset - requery..\n", retry);
                tmcReset(dev);
                goto tsc_retry;
            }
        }
        ERROR("write failed (%s)\n", strerror(errno));
    }
    if(ret != strlen(cmd)){
        ERROR("Failed to write sufficient number of bytes\n");
    }

    ret = read(dev->fd, reqrsp, 50000-1);

    if(ret == -1){

        if(errno == ETIMEDOUT){
            if(--retry < 0){
                ERROR("measurement read failed (%s)\n", strerror(errno));
            }else{
                INFO("Timeout (retry %d). Attempting reset - requery..\n", retry);
                tmcReset(dev);
                goto tsc_retry;
            }
        }
        ERROR("measurement read failed (%s)\n", strerror(errno));
    }else if(ret == 0){
        ERROR("Response was zero.\n");
    }

    reqrsp[ret-1] = '\0';

    if(rsp == NULL){
//            INFO("%s returned %s (%d)\n", cmd, reqrsp, ret);
        printw("%s", reqrsp);
    }
    else{
        strcpy(rsp, reqrsp);
    }
    return ret;
}


void tmcScpiCmd(struct tmcdev *dev, char* cmd, int response, char *rsp){

    char reqrsp[50000];
    int ret;
    int retry = 3;

tsc_retry:
    ret = write(dev->fd, cmd, strlen(cmd));
    if(ret == -1){
       if(errno == ETIMEDOUT){
            if(--retry < 0){
                ERROR("measurement read failed (%s)\n", strerror(errno));
            }else{
                INFO("Timeout write (retry %d). Attempting reset - requery..\n", retry);
                tmcReset(dev);
                goto tsc_retry;
            }
        }
        ERROR("tmcScpiCmd write failed (%s)\n", strerror(errno));
    }
    if(ret != strlen(cmd)){
        ERROR("Failed to write sufficient number of bytes\n");
    }

    if(response){
        ret = read(dev->fd, reqrsp, 50000-1);

        if(ret == -1){

            if(errno == ETIMEDOUT){
                if(--retry < 0){
                    ERROR("measurement read failed (%s)\n", strerror(errno));
                }else{
                    INFO("Timeout read (retry %d). Attempting reset - requery..\n", retry);
                    tmcReset(dev);
                    goto tsc_retry;
                }
            }
            ERROR("measurement read failed (%s)\n", strerror(errno));
        }else if(ret == 0){
            ERROR("Response was zero.\n");
        }

        reqrsp[ret] = '\0';

        if(rsp == NULL){
            INFO("%s returned %s (%d)\n", cmd, reqrsp, ret);
//            printw("%s", reqrsp);
        }
        else{
            strcpy(rsp, reqrsp);
        }
    }
}


void initTmcDev(struct tmcdev **dev_h, char* name, char* d_file){

    struct tmcdev *dev;
    
    *dev_h = malloc(sizeof(struct tmcdev));
    dev = *dev_h;

    dev->name = malloc(strlen(name)+1);
    strcpy(dev->name, name);

    dev->d_file = malloc(strlen(d_file)+1);
    strcpy(dev->d_file, d_file);

    dev->fd = open(dev->d_file, O_RDWR);
    if(dev->fd < 0){
        ERROR("Could not open %s (%s)\n", d_file, strerror(errno));
    }

//    tmcReset(dev); // Always clear before communication

//    INFO("Initialised usbtmc device %s on path %s.\n", dev->name, dev->d_file);
}

void destroyTmcDev(struct tmcdev *dev){

    close(dev->fd);
    free(dev->d_file);
    free(dev->name);
}


