
#include <stdio.h>
#include <powerlog/message.h>
#include <signal.h>
#include <getopt.h>
#include <fcntl.h>
#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netdb.h>

#define MODULE "CLIENT"

#define LOG(...)\
{\
    fprintf(stdout, "[LOG][%s]\t",  MODULE);\
    fprintf(stdout, __VA_ARGS__);\
}


#define ERROR(...)\
{\
    LOG(__VA_ARGS__);\
    fprintf(stderr, "[ERROR]\t[%s]\t", MODULE);\
    fprintf(stderr, __VA_ARGS__);\
    exit(EXIT_FAILURE);\
}

#define INFO(...)\
{\
    LOG(__VA_ARGS__);\
    fprintf(stderr, "[INFO]\t[%s]\t", MODULE);\
    fprintf(stderr, __VA_ARGS__);\
}

typedef 
struct k2280s{

    /* Server */
    int server_fd;
    struct sockaddr_in server_addr;

    /* Logger */
    FILE *log_fp;
    pthread_t log_thread;
    double start_time;
    char name[500];
    char* logname;
    
} k2280s_t;



void k2280s_lstart(void* dev_h){

	k2280s_t *dev;
	struct k2280s_msg msg;
	int ret, to_send;
	char *buf;

	dev = (k2280s_t*) dev_h;

	to_send = strlen(dev->logname);

	msg.id  = MSG_START;
	msg.len = to_send > 0 ? to_send + 1 : 0;
	INFO("Message length %d (%lu)\n", msg.len, sizeof(struct k2280s_msg));

	buf = (char*) malloc(sizeof(struct k2280s_msg) + strlen(dev->logname) + 1);
	bcopy((void*) &msg, buf, sizeof(struct k2280s_msg));
	bcopy(dev->logname, buf+sizeof(struct k2280s_msg), strlen(dev->logname)+1);

	INFO("Initiating remote power measurements..\n");

	ret = write(dev->server_fd, (void*) buf, sizeof(struct k2280s_msg)+strlen(dev->logname)+1);
	if(ret != (int) (sizeof(struct k2280s_msg) + strlen(dev->logname) + 1)){
		ERROR("Unable to write log start message (wrote %d bytes, %s). Exiting..\n", ret, strerror(errno));
	}

	ret = read(dev->server_fd, (void*) &msg, sizeof(struct k2280s_msg));
	if(ret == -1){
		ERROR("Unable to read server start response\n");
	}
	else if (ret < (int) sizeof(struct k2280s_msg)){
		ERROR("Could not read required size of response to server start (%d, expected %lu)\n", ret, sizeof(struct k2280s_msg));
	}

	if(msg.id != MSG_TXLOG){
		ERROR("Invalid response to server start (%d)\n", msg.id);
	}

	free(buf);
	INFO("Remote power measurements initiated.\n");

}

#define RX_BSZ 50000
void k2280s_lstop(void* dev_h){

	k2280s_t *dev;
	struct k2280s_msg msg;
	int ret, to_write, block_w, to_read, read_n, i;
	FILE *log_fp;
	char rxbuf[RX_BSZ];

	dev = (k2280s_t*) dev_h;

	msg.id  = MSG_STOP;
	msg.len = 0;

	INFO("Stopping server.\n");

	/* Signal to logger that we are done, and would kindly
	 * like the log to be transferred. */
	ret = write(dev->server_fd, (void*)&msg, sizeof(struct k2280s_msg));
	if(ret != sizeof(struct k2280s_msg)){
		ERROR("Unable to write log start message (wrote %d bytes, %s). Exiting..\n", ret, strerror(errno));
	}

	INFO("Server stopped\n");

	/* Log length in message */
	ret = read(dev->server_fd, (void*)&msg, sizeof(struct k2280s_msg));
	if(ret != sizeof(struct k2280s_msg)){
		ERROR("Unable to read length of log (read %d bytes, %s). Exiting..\n", ret, strerror(errno));
	}

	INFO("To read %d bytes\n", msg.len);

	log_fp = fopen("power.log", "w");

	to_read = msg.len;
	read_n = 0;
	block_w = 0;

	INFO("Reading power log\n");
	INFO("0 > ");

	/* Receive the log and write it to disk */
	do{

		//        fprintf(stderr, "Reading...\n");
		ret = read(dev->server_fd, (void*)rxbuf, RX_BSZ);
		if(ret == -1){
			ERROR("Unable to read log (%s). Exiting..\n", strerror(errno));
		}

		//        fprintf(stderr, "Read %d bytes\n", ret);

		to_write = ret;

		ret = fwrite(rxbuf, 1, to_write, log_fp);
		if(ret != to_write){
			ERROR("Unable to write log (%s). Exiting..\n", strerror(errno));
		}

		read_n += to_write;
		for(i=block_w; i<((20 * read_n) / to_read); i++){
			fprintf(stdout, "-");
			block_w++;
		}

		//        fprintf(stderr, "Wrote %d bytes\n", ret);

		msg.len -= to_write;
	}while(msg.len > 0);

	fprintf(stdout, " < 100\n");

	/* Done receiving, close and return */
	fclose(log_fp);

}

void k2280s_destroy(void* dev_h){

	k2280s_t *dev;

	dev = (k2280s_t*) dev_h;

	close(dev->server_fd);

	free(dev->logname);
	free(dev_h);
}

void k2280s_init(char* hostname, int port, void ** dev_h){

	k2280s_t *dev;
	char *logname = "testrun";
	struct hostent *server = NULL;

	*dev_h              = (void*) calloc(1, sizeof(k2280s_t));
	dev = (k2280s_t*) *dev_h;

	strcpy(dev->name, "k2280s");

	INFO("Connecting to K2280S %s (port %d), logname %s\n", hostname, port, logname);

	server = gethostbyname(hostname);
	if(server == NULL){
		ERROR("Unable to resolve hostname. Exiting..\n");
	}

	dev->server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(dev->server_fd < 0){
		ERROR("Unable to create socket (%s). Exiting..\n", strerror(errno));
	}

	bzero((char*) &dev->server_addr, sizeof(struct sockaddr_in));
	dev->server_addr.sin_family = AF_INET;
	bcopy(
			(char*) server->h_addr, 
			(char*) &dev->server_addr.sin_addr.s_addr,
			server->h_length);
	dev->server_addr.sin_port = htons(atoi(port));

	if(connect(
				dev->server_fd, 
				(struct sockaddr*) &dev->server_addr, 
				sizeof(struct sockaddr)) == -1){
		ERROR("Unable to connect to server (%s). Exiting..\n", strerror(errno));
	}

	dev->logname = (char*) malloc(strlen(logname)+1);
	strcpy(dev->logname, logname);

	INFO("Success!\n");
}

void print_help(void){

	fprintf(stdout, "k2280s external logger client frontend\n");
	fprintf(stdout, "\tCalled with -s <server_hostname> -p <port>\n");
}


static volatile int keepRunning = 1;

void intHandler(int dummy) {
    keepRunning = 0;
}

int main(int argc, char** argv){

	char c;
	int optind, row = 0, ret;

	struct option options[] = 
	{
		{"server",  required_argument, 0, 's'},
		{"port",    required_argument, 0, 'p'},
		{"help",    optional_argument, 0, 'h'},
		{0,0,0,0}
	};

	int port;
	char server_hostname[512];

    int ready = 2;

	while((c = getopt_long(argc, argv, "s:p:h", options, &optind)) != -1){

		switch(c){
			case 'p':
                ready--;
				port = atoi(optarg);
				break;

			case 's':
                ready--;
                fprintf(stderr, "optarg: %s\n", optarg);
				strcpy(&server_hostname[0], optarg);
				break;

			case 'h':
				print_help();
				break;
		}
	}


    if( ready )
    {
        print_help();
        exit(0);
    }

	INFO("Connecting to %s : %d\n", server_hostname, port);
    
    void * dev_h;
    k2280s_init( server_hostname, port, &dev_h );

    k2280s_lstart( dev_h );

   signal(SIGINT, intHandler);

    while (keepRunning);

    k2280s_lstop( dev_h );

	return 0;
}


