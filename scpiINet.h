
#ifndef SCPIINET
#define SCPIINET

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <errno.h>
#include <string.h>

#include "common.h"

#define RESPONSE 1
#define NO_RESPONSE 0

struct scpiINetDev{
    char* name;     // Name of device (nickname)

    int sockfd;
};

int scpiINetWrite(struct scpiINetDev *dev, char* cmd);
int scpiINetRead(struct scpiINetDev *dev, char* cmd, char *rsp);

void initScpiINetDev(struct scpiINetDev **dev_h, char* name, char* d_file);
void destroyScpiINetDev(struct scpiINetDev *dev_h);

#endif // SCPIINET
