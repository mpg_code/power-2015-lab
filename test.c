
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <errno.h>

int main(void){
    
    int portno;
    int sockfd;

    struct hostent *server;
    struct sockaddr_in serv_addr;

    int ret;
    char rve[8];
    char buf[5000];

    rve[0] = 0xf0;
    rve[1] = 0xf0;
    rve[2] = 0xf0;
    rve[3] = 0xf0;

    rve[4] = 0x04;
    rve[5] = 0x01;
    rve[6] = 0x06; // Size
    rve[7] = 0x00;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0)
        error("Error opening socket");
    portno = 5051;

    server = gethostbyname("10.0.0.2");
    if(server==NULL)
        error("NO SUCH HOST");

    bzero((char*) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy(
            (char*) server->h_addr,
            (char*) &serv_addr.sin_addr.s_addr,
            server->h_length
         );
    serv_addr.sin_port = htons(portno);

    if(connect(sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0)
        error("Error connecting");

    ret = read(sockfd, buf, 5000);
    if(ret == -1)
        fprintf(stdout, "Read %d, %s\n", ret, strerror(errno));

    fprintf(stdout, "Got %s\n", buf);

    ret = write(sockfd, rve, 8);
    if(ret == -1)
        fprintf(stdout, "Wrote %d, %s\n", ret, strerror(errno));

    fprintf(stdout, "Write idn\n");
    ret = write(sockfd, "*IDN?\n", 6);
    if(ret == -1)
        fprintf(stdout, "Wrote %d, %s\n", ret, strerror(errno));

    fprintf(stdout, "Reading\n");

    ret = read(sockfd, buf, 5000);
    if(ret == -1)
        fprintf(stdout, "Read %d, %s\n", ret, strerror(errno));

    fprintf(stdout, "Got (%d) %s\n", ret, &buf[8]);

    close(sockfd);

    return 0;
}
