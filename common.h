
#ifndef COMMON
#define COMMON

#include <ncurses.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>

extern FILE *logg;
extern struct timespec clock_s;
extern pthread_mutex_t logg_lock;
extern pthread_mutex_t ncl;

// Convenience macro to stop logging to stdout
#if 0
#define LOG(...)\
    
#else
#define LOG(...)\
{\
    struct timespec clock_l;\
    clock_gettime(CLOCK_REALTIME, &clock_l);\
    long elapsed_ms_l = ((long) clock_l.tv_sec  * (long)1e3  + \
                       (long) clock_l.tv_nsec / (long)1e6) - \
                        ((long) clock_s.tv_sec  * (long)1e3  + \
                         (long) clock_s.tv_nsec / (long)1e6);\
    pthread_mutex_lock(&logg_lock);\
    logg = fopen("./k2280s_debug.txt", "a");\
    fprintf(logg, "[LOG]\t%ld\t[%s]\t", elapsed_ms_l, MODULE);\
    fprintf(logg, __VA_ARGS__);\
    fclose(logg);\
    pthread_mutex_unlock(&logg_lock);\
}

#endif

#define ERROR(...)\
{\
    LOG(__VA_ARGS__);\
    fprintf(stderr, "[ERROR]\t[%s]\t", MODULE);\
    fprintf(stderr, __VA_ARGS__);\
    endwin();\
    exit(EXIT_FAILURE);\
}

#define INFO(...)\
{\
    LOG(__VA_ARGS__);\
    fprintf(stderr, "[INFO]\t[%s]\t", MODULE);\
    fprintf(stderr, __VA_ARGS__);\
}


#endif // COMMON
