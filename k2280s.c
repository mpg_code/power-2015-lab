
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <ncurses.h>
#include <string.h>
#include <semaphore.h>

#include "k2110.h"
#include "k2280s.h"
#include "common.h"
#include "message.h"

#define MODULE "K2280S"
#define TXBUF_SIZE 50000

extern struct k2110 * vmons[10];
extern int num_vmons;

struct psmp{
    double tstamp;
    float v, c;
    float pow;
};

static int buffer_reader_active;
char response_v1[300000];
char response_v2[300000];
char *buffer_data;
sem_t data_avail, buffer_free;

static void* buffer_reader(void *args){

    struct k2280s *mon = (struct k2280s *) args;
    struct scpiINetDev *sdev = mon->sdev;
    char *request = ":TRACE1:DATA? \"READ,SOUR,TIME\"\n";
    int first_read = 1;
    char *r_l;
    int ret;

    /* We rewrite NPLC to try to avoid a hardware issue
     * where the measurements are som ten times larger
     * than normal */
    scpiINetWrite(sdev, ":SENSE1:CURR:NPLC 0.010\n");
    scpiINetWrite(sdev, ":SENSE1:CURR:NPLC 0.009\n");

    scpiINetWrite(sdev, ":INITIATE1:CONT ON\n");

    while(buffer_reader_active){
        if(first_read){

            r_l = response_v1;

            do{
                LOG("TRACE1:CLEAR\n");
                scpiINetWrite(sdev, ":TRACE1:CLEAR\n");

                LOG("BUFFER READ\n");
                ret = scpiINetRead(sdev, request, r_l);
                LOG("Buffer read returned %d bytes \n", ret);

                if(ret == 1){ // Attempt rescue
                    LOG("Attempting rescue, ret  was 1\n");
                    scpiINetWrite(sdev, ":SENSE1:CURR:NPLC 0.010\n");
                    scpiINetWrite(sdev, ":SENSE1:CURR:NPLC 0.009\n");
                }
            }while(ret == 1);

            pthread_barrier_wait(&mon->barrier);
            first_read = 0;
        }else{
            if(r_l == response_v1)
                r_l = response_v2;
            else
                r_l = response_v1;

            ret = scpiINetRead(sdev, request, r_l);
            LOG("Buffer read returned %d bytes \n", ret);

            if(ret == 1){ // Can happen if buffer was empty at reading
                LOG("Buffer read returned 1\n");
                continue;
            }

        }

        sem_wait(&buffer_free);
        buffer_data = r_l;
        sem_post(&data_avail);
    }

    return NULL;
}

static void* pull_data(void *args){

    int i, maxi;
    int ret;
    int to_read;
    double newest_ts = -1.0F;
    double newest_ts_l = -1.0F;
    double first_ts = -1.0F;
    double p_ts = -1.0F;
    int p_times;
    int n_ts_pos = 0;
    int ts_pos_pre = 0;
    int first_run = 1;
    int n_samples = 0;
    int exit_at_end = 0;
    pthread_t scpi_thread;
    void *retval;

    float j;

    struct psmp samples[2500];

//    char *request = ":TRACE1:DATA:SELECTED? 1, 10, \"READ,SOUR,TIME\"";
    char *response;
    char response2[300000];
    char sample_string[500];
    
    long elapsed_ms;

    char *token, *sample;

    struct timespec time1, time2;

    struct k2280s *mon = (struct k2280s *) args;
    struct scpiINetDev *sdev = mon->sdev;

    sem_init(&data_avail, 0, 0); // Variable, thread-shared, initial value
    sem_init(&buffer_free, 0, 0); // Variable, thread-shared, initial value

    buffer_reader_active = 1;
    if(pthread_create(&scpi_thread, NULL, buffer_reader, (void*) mon)){
        ERROR("Could not create data thread (%s)\n", strerror(errno));
    }

    mon->log_size = 0;

    for(i=0;i<2500; i++)
        samples[i].tstamp = -1;

    i = 0;

    //scpiINetWrite(sdev, ":SENSE1:CURRENT:NPLC 0.032\n");
/*
    scpiINetWrite(sdev, ":TRACE1:CLEAR\n");
    scpiINetWrite(sdev, ":SENSE1:CURRENT:NPLC 5.5\n");
    */

    pthread_mutex_lock(&ncl);
    mvwprintw(mon->win, 6, 1, "  Last Buffer Readout / Location    ");
    mvwprintw(mon->win, 7, 1, " 0 % <                       > 100 %");
    mvwprintw(mon->win, 9,  1, "Samples:\t\t\t");
    mvwprintw(mon->win, 10, 1, "Timestamp\t\t\t");
    mvwprintw(mon->win, 11, 1, "Log size:\t\t\t");
    mvwprintw(mon->win, 11, 1, "Num k2110 %d first name %s", num_vmons, vmons[0]->name);
    wrefresh(mon->win);
    pthread_mutex_unlock(&ncl);

    while(1){

        if(!mon->log_active)
            exit_at_end = 1;

        // Make voltage readings
        
        float avg_volt[10];
        int cur_vmeas;
        for(cur_vmeas = 0; cur_vmeas < num_vmons; cur_vmeas++)
        {
            avg_volt[cur_vmeas] = k2110_read_avg( vmons[cur_vmeas] );
        }


        /* Request buffer data */
        clock_gettime(CLOCK_REALTIME, &time1);

        sem_post(&buffer_free); // Signal that data buffer is ready
        sem_wait(&data_avail); // When we return from here, we have more input data

        response = buffer_data;

//        LOG("Buffer read returned %d bytes (%s)\n", ret, response);


        clock_gettime(CLOCK_REALTIME, &time2);

        elapsed_ms = ((long) time2.tv_sec  * (long)1e3  + 
                           (long) time2.tv_nsec / (long)1e6) -
                          ((long) time1.tv_sec  * (long)1e3  +
                           (long) time1.tv_nsec / (long)1e6);

        LOG("Elapsed %ld ms\n", elapsed_ms);

        bcopy(response, response2, 300000);

        sample = response;

        /* The first token must have 'sample' as input
         * parameter. We just continue scanning the whole
         * buffer until we're done. */
        token = strtok(sample, ",");

        do{
            float current, voltage;
            char srg[100];
            double ms = 0.0F;

            if(i > 2499){ // Buffer is full, start re-reading
                LOG("Buffer full, re-read starting\n");
                token = strtok(response2, ",");
                i = 0;
            }

            // Current
            current = atof(token); // token should be set

            // Voltage
            token = strtok(NULL, ",");
            voltage = atof(token);

            // Timestamp
            token = strtok(NULL, ",");

            LOG("Tstamp %s\n", token);
//            fprintf(stdout, "v %f c %f, string %s\n", voltage, current, response2);

            /* Dirty software solution for even uglier
             * hardware problem. 
             *
             * :<
             * */

            // Hours
            srg[0] = token[0];
            srg[1] = token[1];
            srg[2] = '\0';

            ms += atof(srg) * 3600;

            // Minutes
            srg[0] = token[3];
            srg[1] = token[4];
            srg[2] = '\0';

            ms += atof(srg) * 60;

            // Seconds
            srg[0] = token[6];
            srg[1] = token[7];
            srg[2] = '\0';

            ms += atof(srg);
            ms *= 1000;

            // Milliseconds
            srg[0] = token[9];
            srg[1] = token[10];
            srg[2] = token[11];
            srg[3] = token[12];
            srg[4] = '\0';

            ms += atof(srg) / 10.0F;

            if(first_ts == -1.0F){
                first_ts  = ms;
                p_ts = ms;
                p_times = 1;
            }

            /* Theres a bug in the k2280s where timestamps
             * sometimes gets shifted up or down by x minutes for
             * about ten samples */
            if(abs(ms - p_ts) > 4000){

                ms = p_ts + 0.7 * p_times++; // Give a manual boost to last known correct timestamp
                LOG("[WARNING] Autocorrecting timestamp to %f\n", ms);
            }else{

                p_ts = ms;
                p_times = 1;
            }

            ms -= first_ts;

            if(newest_ts == -1.0F)
                newest_ts = ms;

            LOG("ms %f first_ts %f newest_ts %f", ms, first_ts, newest_ts);


            samples[i].tstamp = ms;
            samples[i].c      = current;
            samples[i].v      = voltage;
            samples[i].pow    = (current * voltage);
            i++;

            // Attempt to find next sample
            token = strtok(NULL, ",");
        }while(token != NULL);

        LOG("%d samples\n", i);
        if(first_run){
            if(i == 2500){
                pthread_mutex_lock(&ncl);
                mvwprintw(mon->win, 15, 1, "WARNING: BUFFER FULL AT START");
                pthread_mutex_unlock(&ncl);
            }
            first_run = 0;
        }

        maxi = 2500;

        LOG("2499 ts %f, 0 ts %f\n",   samples[2499].tstamp, samples[0].tstamp);
        LOG("2499 pow %f, 0 pow %f\n", samples[2499].pow, samples[0].pow);

        /* If this is true --- we have a wraparound 
         *
         * differing samples with same timestamps have been
         * seen */
        if(
                   samples[2499].tstamp != -1                   // Buffer full
                && samples[2499].tstamp <= samples[0].tstamp    // Wraps (timestamps can appear twice)
                && samples[0].tstamp > newest_ts                // Newest reading at least at start
                ){
            LOG("Wraparound\n");
            // Need to find where we "left off"
            for(i=0; i<2500; i++){
                if(samples[i].tstamp == newest_ts){
                    LOG("Wrap ts %f (%d)\n", newest_ts, i);
                    maxi = i;
                    i++;
                    break;
                }
            }

            for(; i<2500; i++){

                newest_ts_l = samples[i].tstamp;
                n_ts_pos = i;

                // Generate string that looks like the one
                // produced with INA219 logger
                int ret;
                ret = sprintf(sample_string, "%f,%f", samples[i].tstamp, samples[i].pow);

                for(cur_vmeas = 0; cur_vmeas < num_vmons; cur_vmeas++)
                {
                    ret += sprintf(sample_string + ret, ",%f", avg_volt[cur_vmeas]);

                }

                sprintf(sample_string + ret, "\n");


                n_samples++;
                ret = fwrite(sample_string, 1, strlen(sample_string), mon->log_fp);
                if(ret != strlen(sample_string)){
                    ERROR("fwrite() %s\n", strerror(errno));
                }

                mon->log_size += ret;
            }
        }

        /* Else, or in the default case, just store readings
         * as normal from the beginning */
        for(i=0; i<maxi; i++){

            if(samples[i].tstamp == -1)
                break;

            if(samples[i].tstamp >= newest_ts){

                newest_ts_l = samples[i].tstamp;
                n_ts_pos = i;

                int ret;
                ret = sprintf(sample_string, "%f,%f", samples[i].tstamp, samples[i].pow);

                for(cur_vmeas = 0; cur_vmeas < num_vmons; cur_vmeas++)
                {
                    ret += sprintf(sample_string + ret, ",%f", avg_volt[cur_vmeas]);

                }

                sprintf(sample_string + ret, "\n");


                n_samples++;
                ret = fwrite(sample_string, 1, strlen(sample_string), mon->log_fp);
                if(ret != strlen(sample_string)){
                    ERROR("fwrite() %s\n", strerror(errno));
                }

                mon->log_size += ret;
            }
        }

        // Store newest reading from this scan
        if(newest_ts > newest_ts_l){
            ERROR("newest ts %f, local %f\n", newest_ts, newest_ts_l);
        }
        newest_ts = newest_ts_l;
        LOG("Newest ts %f, pos (%d)\n", newest_ts, n_ts_pos);

        float progress = n_ts_pos / 2500.0F;
        float progress_p = ts_pos_pre / 2500.0F;

        pthread_mutex_lock(&ncl);
        mvwprintw(mon->win, 7, 8, "                      ");
        for(j=0.0F; j<20.0F; j+=1.0){

            if(progress < progress_p){ // Wraparound
                
                if((j/20.0F) >= progress_p)
                    mvwprintw(mon->win, 7, 8+(int)j, "-");
                
                if((j/20.0F) <= progress)
                    mvwprintw(mon->win, 7, 8+(int)j, "-");
            }
            else{
                if((j/20.0F) >= progress_p && (j/20.0F) <= progress){
                    mvwprintw(mon->win, 7, 8+(int)j, "-");
                }
            }

            if((j/20.0F) < progress && ((j+1.0)/20.0F) >= progress){
                mvwprintw(mon->win, 7, 8+(int)j, ">");
            }
        }

        mvwprintw(mon->win, 9,  1, "Samples:\t%d",  n_samples);
        mvwprintw(mon->win, 10, 1, "Timestamp\t%f", newest_ts);
        mvwprintw(mon->win, 11, 1, "Log size:\t%d", mon->log_size);


        wrefresh(mon->win);
        pthread_mutex_unlock(&ncl);

        ts_pos_pre = n_ts_pos;


        LOG("Log size %d bytes\n", mon->log_size);
        clock_gettime(CLOCK_REALTIME, &time2);

        elapsed_ms = ((long) time2.tv_sec  * (long)1e3  + 
                           (long) time2.tv_nsec / (long)1e6) -
                          ((long) time1.tv_sec  * (long)1e3  +
                           (long) time1.tv_nsec / (long)1e6);

//        usleep(500 * 1000);
        if(elapsed_ms > 100)
            LOG("Elapsed %ld ms\n", elapsed_ms);

        if(exit_at_end)
            break;
    }

    buffer_reader_active = 0;
    sem_post(&buffer_free);

    if(pthread_join(scpi_thread, &retval)){
        ERROR("Could not rejoin data thread (%s)\n", strerror(errno));
    }

    sem_destroy(&buffer_free);
    sem_destroy(&data_avail);

    pthread_mutex_lock(&ncl);
    mvwprintw(mon->win, 7, 1, " 0 % <                        > 100 %");
    wrefresh(mon->win);
    pthread_mutex_unlock(&ncl);

    LOG("USB poll thread stopped.\n");

    return NULL;
}

static void* server_thread(void *args){

    struct k2280s *dev = (struct k2280s *) args;
    struct scpiINetDev* sdev = dev->sdev;

    struct k2280s_msg *msg, retmsg;

    char buf[sizeof(struct k2280s_msg)];
    char rsp[1000];

    int ret;
    int idx;

    void *retval;

    msg = (struct k2280s_msg*) buf;

    while(1){

        dev->has_client = 0;

        LOG("Initiate continuous off / trace clear\n");
        scpiINetWrite(sdev, ":INITIATE1:CONTINUOUS OFF\n");
        scpiINetWrite(sdev, ":INITIATE1:CONTINUOUS ON\n");


        LOG("Waiting for client..\n");
        pthread_mutex_lock(&ncl);
        mvwprintw(dev->win, 2, 1, "Waiting for incoming connection...         ");
        mvwprintw(dev->win, 3, 1, "Client                                      ");
        wrefresh(dev->win);
        pthread_mutex_unlock(&ncl);
        dev->client_addr_len = sizeof(dev->client_addr);
        dev->client_fd = accept( // Blocks here, waiting for connection
                dev->server_fd, 
                (struct sockaddr *) &dev->client_addr, 
                &dev->client_addr_len);

        if(dev->client_fd == -1){
            ERROR("Server couldn't accept (%s). Exiting..\n", strerror(errno));
        }

        dev->has_client = 1;

        pthread_mutex_lock(&ncl);
        mvwprintw(dev->win, 2, 1, "Connected                                 ");
        mvwprintw(dev->win, 3, 1, "Client %s                  ", inet_ntoa(dev->client_addr.sin_addr));
        wrefresh(dev->win);
        pthread_mutex_unlock(&ncl);
        LOG("Client connected from %s..\n", inet_ntoa(dev->client_addr.sin_addr));

        /* Create temporary log, reopen */
        dev->log_fp = fopen("log.csv", "w");
        if(dev->log_fp == NULL){
            ERROR("Could not fopen (%s)\n", strerror(errno));
        }
        fclose(dev->log_fp);

        dev->log_fp = fopen("log.csv", "r+");
        if(dev->log_fp == NULL){
            ERROR("Could not f(re)open (%s)\n", strerror(errno));
        }

        while(dev->has_client){

            idx = 0;

            /* Read message */
            do{
                usleep(50000);
                LOG("Reading message\n");
                ret = read(dev->client_fd, (void*)&buf[idx], sizeof(struct k2280s_msg)-idx);
                if(ret == -1){
                    if(errno == EBADF){
                        LOG("WARNING: Bad file descriptor, assuming client disconnected\n");
                        dev->has_client = 0;
                        close(dev->client_fd);
                        break;
                    }
                    ERROR("Error reading message 1 (%d, %s)\n", errno, strerror(errno));
                }
                else if(ret == 0){
                    LOG("Zero byte message.\n");
                    close(dev->client_fd);
                    dev->has_client = 0;
                }
                LOG("Received message %d bytes\n", ret);

                idx += ret;
            }while(idx != sizeof(struct k2280s_msg));

            if(!dev->has_client){
                dev->log_active = 0;
                if(pthread_join(dev->dthread, &retval)){
                    LOG("[WARNING] Could not rejoin data thread (%s)\n", strerror(errno));
                }
                break;
            }

            switch(msg->id){
                case MSG_START:

                    LOG("Initiating measurements.\n");
                    pthread_mutex_lock(&ncl);
                    mvwprintw(dev->win, 2, 1, "Measurements starting...               ");
                    wrefresh(dev->win);
                    pthread_mutex_unlock(&ncl);

                    /* Test ID */
                    LOG("Message ID length %u (%lu)\n", msg->len, sizeof(struct k2280s_msg));
                    if(msg->len > 0){

                        char testid[5000];

                        idx = 0;

                        /* Read message */
                        do{
                            ret = read(dev->client_fd, (void*) &testid[idx], sizeof(testid)-idx);
                            if(ret == -1){
                                ERROR("Error reading message 2 (%s)\n", strerror(errno));
                            }

                            idx += ret;
                        }while(idx != msg->len);

                        LOG("TEST ID %s\n", testid);
                        pthread_mutex_lock(&ncl);
                        mvwprintw(dev->win, 4, 1, "TEST ID  <%s>         ", testid);
                        wrefresh(dev->win);
                        pthread_mutex_unlock(&ncl);

                    }

//                    scpiINetWrite(sdev, ":INITIATE1:CONTINUOUS ON\n");
                    pthread_mutex_lock(&ncl);
                    mvwprintw(dev->win, 2, 1, "Measuring power.               ");
                    wrefresh(dev->win);
                    pthread_mutex_unlock(&ncl);
//                    scpiINetWrite(sdev, ":INITIATE1:CONTINUOUS?\n");

                    dev->log_active = 1;

                    /* Start pulling measurements */
                    if(pthread_create(&dev->dthread, NULL, pull_data, (void*) dev)){
                        ERROR("Could not create data thread (%s)\n", strerror(errno));
                    }

                    /* Wait for first measurements to
                     * arrive, before sending GO to
                     * application */
                    LOG("Waiting\n");
                    pthread_barrier_wait(&dev->barrier);
                    LOG("Complete\n");

                    msg->id = MSG_TXLOG;
                    ret = write(dev->client_fd, (void*) msg, sizeof(struct k2280s_msg));
                    if(ret == -1 || ret < sizeof(struct k2280s_msg)){
                        ERROR("Unable to send measurement start reply (%d)\n", ret);
                    }
                    LOG("COMPLEEETE\n");

                    break;

                case MSG_STOP:

                    /* Stop logging */
                    LOG("Stopping measurement logging.\n");
                    pthread_mutex_lock(&ncl);
                    mvwprintw(dev->win, 2, 1, "Stopping measurements..               ");
                    wrefresh(dev->win);
                    pthread_mutex_unlock(&ncl);
                    dev->log_active = 0;
                    if(pthread_join(dev->dthread, &retval)){
                        ERROR("Could not rejoin data thread (%s)\n", strerror(errno));
                    }

//                    scpiINetWrite(sdev, ":INITIATE1:CONTINUOUS OFF\n");
//                    scpiINetWrite(sdev, ":TRACE1:CLEAR\n");


                    /* Transmitting log */
                    pthread_mutex_lock(&ncl);
                    mvwprintw(dev->win, 2, 1, "Measurements stopped, transmitting log. ");
                    wrefresh(dev->win);
                    pthread_mutex_unlock(&ncl);
                    LOG("Transmitting log.\n");
                    retmsg.id = MSG_TXLOG;
                    retmsg.len = dev->log_size;


                    // Log size
                    ret = write(dev->client_fd, (void*)&retmsg, sizeof(struct k2280s_msg));
                    if(ret == -1){
                        ERROR("Write log size failed (%s)\n", strerror(errno));
                    }
                    if(ret != sizeof(struct k2280s_msg)){
                        ERROR("Could not write log size\n");
                    }

                    // Actual log
                    int to_tx = dev->log_size;
                    int bsz;
                    if(-1 == fseek(dev->log_fp, SEEK_SET, 0)){
                        ERROR("Unable to fseek() (%s)\n", strerror(errno));
                    }
                    do{
                        char txbuf[TXBUF_SIZE];
                        bsz = ret = fread(
                                txbuf, 
                                1, 
                                TXBUF_SIZE <= to_tx ? TXBUF_SIZE : to_tx, 
                                dev->log_fp);

                        if(ret == -1){
                            ERROR("Unable to read tx log (%s)\n", strerror(errno));
                        }
                        if(ret == 0){
                            ERROR("Unable to read tx log, zero (%s)\n", strerror(errno));
                        }

                        LOG("Transmitting %d bytes\n", bsz);
                        idx = 0;
                        do{
                            ret = write(dev->client_fd, (void*)&txbuf[idx], bsz-idx);
                            if(ret == -1){
                                ERROR("Unable to write tx log (%s)\n", strerror(errno));
                            }
                            idx += ret;
                        }while(idx != bsz);

                        LOG("To transmit %d bytes\n", to_tx);

                        to_tx -= bsz;
                    }while(to_tx > 0);

                    pthread_mutex_lock(&ncl);
                    mvwprintw(dev->win, 2, 1, "Disconnecting client..                     ");
                    mvwprintw(dev->win, 4, 1, "                                      ");
                    wrefresh(dev->win);
                    pthread_mutex_unlock(&ncl);

                    /* Disconnect from client */
                    close(dev->client_fd);

                    dev->has_client = 0;

                    break;

                default:

                    ERROR("Unvalid message id 0x%x\n", msg->id);
                    break;
            }

        }

        fclose(dev->log_fp);
    }

    return NULL;
}

void k2280s_init(struct k2280s** ddev, int port, char* name, char* inetaddr){

    struct k2280s *dev;
    char buf[5000];
    int opt = 1;

    *ddev = malloc(sizeof(struct k2280s));
    dev = *ddev;

    dev->s_port = port;
    pthread_barrier_init(&dev->barrier, NULL, 2);

    initScpiINetDev(&dev->sdev, name, inetaddr);
    scpiINetRead(dev->sdev, "*IDN?\n", buf);

    LOG("Communicating with %s\n", buf);

    //scpiINetWrite(dev->sdev, "*RCL 1\n");
    scpiINetWrite(dev->sdev, ":TRACE1:POINTS 2500\n");
    scpiINetRead(dev->sdev, ":TRACE1:POINTS?\n", buf);

    LOG("Buffer %s\n", buf);

    dev->server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(dev->server_fd < 0){
        ERROR("Unable to create server socket. Exiting..\n");
    }

    dev->server_addr.sin_family = AF_INET;
    dev->server_addr.sin_addr.s_addr = INADDR_ANY;
    dev->server_addr.sin_port = htons(dev->s_port);

    dev->client_addr_len = sizeof(dev->client_addr);

    if(bind(dev->server_fd, (struct sockaddr *) &dev->server_addr, sizeof(dev->server_addr)) != 0){
        ERROR("Unable to bind socket to address (%s) at port %d. Exiting..\n", strerror(errno), dev->s_port);
    }

    if(listen(dev->server_fd, 10) != 0){
        ERROR("Unable to start listening. Exiting..\n");
    }

    /* Create a window */
    dev->win = newwin(50, 90, 5,5);
    box(dev->win, 0, 0);
    pthread_mutex_lock(&ncl);
    mvwprintw(dev->win, 2, 1, "K2280S initialised.");
    wrefresh(dev->win);
    pthread_mutex_unlock(&ncl);
    curs_set(0);

    /* Create a thread to accept incoming connections */
    if(pthread_create(&dev->sthread, NULL, server_thread, (void*) dev)){
        ERROR("Could not create server thread (%s)\n", strerror(errno));
    }
}

void k2280s_destroy(struct k2280s* dev){
    pthread_barrier_destroy(&dev->barrier);
    close(dev->server_fd);
    destroyScpiINetDev(dev->sdev);
    free(dev);
}


